import os
import warnings

os.environ['FLESHWOUND_ENV_NAME'] = 'client'

from fleshwound.config import current_config
from fleshwound.client import client_loop

with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    client_loop()
    