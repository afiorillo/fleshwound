import os

os.environ['FLESHWOUND_ENV_NAME'] = 'server'

from fleshwound.config import current_config
from fleshwound.server import app

# not recommended, use docker instead
app.run(host='127.0.0.1', port=3001)