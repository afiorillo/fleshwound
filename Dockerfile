FROM python:3.7-alpine
MAINTAINER Andrew Fiorillo andrewmfiorillo@gmail.com
# add just the dependencies
ADD . /fleshwound
WORKDIR /fleshwound
# install the deps
RUN apk add git && pip install -q pipenv &&\
    # setup the python environment
    pipenv sync --bare && pipenv clean --bare
# expose and run
EXPOSE 8000
CMD ["pipenv", "run", "gunicorn", "-c", "gunicorn.conf.py", "--bind", "0.0.0.0:8000", "fleshwound.server:app"]