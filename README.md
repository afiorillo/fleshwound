# Just a Flesh Wound
A monitor that just won't give up.

![It's just a flesh wound, from Monty Python and the Holy Grail](https://upload.wikimedia.org/wikipedia/en/c/c9/Black_Knight_Holy_Grail.png)

Cluster management is messy.
It gets even messier when you have heterogeneous machines and a sneaky IT department.
Face any machine without fear.

## Usage

The `fleshwound` comes in two parts: a server and a client.
Clients ping the server periodically to prove that they're alive and to share any new configuration info.

### Setting up a Server

To setup a server, simply use [Docker](https://www.docker.com/).

```
git clone git@gitlab.com:afiorillo/fleshwound.git
cd fleshwound
docker-compose up
# if you want to bring it up and daemonize
docker-compose up -d
```

This will expose the server at port `8000` and handle a backend database for you.

### Updating a Server

To update a server, you must rebuild the docker-compose stack.

```
# stop the existing stack
docker-compose down
# update the code
git pull
# rebuild with the new code
docker-compose build
# bring stack back up
docker-compose up -d
```

### Setting up a Client

To setup a client, you have a few different options.
The simplest option is to simply clone and use `pipenv`.

```
git clone git@gitlab.com:afiorillo/fleshwound.git
cd fleshwound
pipenv install
pipenv run client
```

The next step would be to wrap the client code in some kind of service wrapper (like [`nssm`](https://nssm.cc/) for Windows).
An example configuration for `nssm` would look like

**TODO**

## Configuration

All configuration is done with environment variables.
The list of configurable variables, default, and purposes for **servers** is:

- `FLESHWOUND_LOG_LEVEL` - `20` - The log level. This follows the enum from Python's [logging](https://docs.python.org/3/library/logging.html#logging-levels). It must be an integer.
- `BEARER_KEY` - \`\` - Acceptable bearer keys for workers to use. This must always match a worker's `BEARER_KEY`.
- `MONGO_URL` - `mongodb://127.0.0.1:27017/` - The MongoDB URI to use for storage. If running in Docker you don't need to worry about this.

The list of configurable variables, defaults, and purposes for **clients** is:

- `FLESHWOUND_LOG_LEVEL` - `20` - The log level. This follows the enum from Python's [logging](https://docs.python.org/3/library/logging.html#logging-levels). It must be an integer.
- `FLESHWOUND_BEARER_KEY` - \`\` - Must match the `BEARER_KEY` of the server.
- `FLESHWOUND_SERVER_URL` - `http://127.0.0.1:8000` - The URL and port to the server.
- `FLESHWOUND_LOOP_INTERVAL` - `60` - The number of seconds between pinging the server. Must be an integer.
- `FLESHWOUND_UPDATE_INTERVAL` - `21600` - The number of seconds between checking for updates. Always checks for an update when first starting up.

## Todo List

- Easier TLS setup
- Tagging releases + pairing with automatic updates
