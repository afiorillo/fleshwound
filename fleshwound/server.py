from datetime import datetime

from flask import Flask, render_template, jsonify, request
from marshmallow import ValidationError

from fleshwound.config import current_config
from fleshwound import db, version
from fleshwound.schema import msgpack_to_info

def make_app(**kwargs):
    app = Flask('fleshwound', static_folder="./static/dist", template_folder="./static")
    app.config.from_object(current_config)

    app.add_url_rule('/', view_func=Views.list_machines, endpoint='index')
    app.add_url_rule('/<hostname>.json', view_func=Views.get_machine_info_json, endpoint='get_machine_info')
    app.add_url_rule('/diff/<hostname>.json', view_func=Views.diff_machine_info, endpoint='diff_machine_info')
    app.add_url_rule('/api/health', view_func=Views.healthcheck, endpoint='healthcheck')
    app.add_url_rule('/api/update', view_func=Views.update_machine, endpoint='update_machine', methods=['PUT'])
    app.add_url_rule('/api/server_version', view_func=Views.server_version, endpoint='server_version')

    @app.errorhandler(500)
    def handle_internal_server_error(e):
        return render_template('500.html'), 500

    return app


class Views(object):
    @staticmethod
    def auth_is_valid():
        authtype, key = request.headers.get('Authorization', '').split(' ')
        return authtype == 'Bearer' and key == current_config.BEARER_KEY

    @staticmethod
    def list_machines():
        # get from db
        machines = db.machine_list()

        # reshape for jinja
        for indx, machineObj in enumerate(machines):
            machineObj['_poll_time'] = machineObj['timestamp'].strftime(current_config.TIME_FMT)
            machineObj['_secs_since_beat'] = (datetime.utcnow() - machineObj['timestamp']).seconds
        return render_template('index.html', machines=machines, constants=current_config.JINJA_CONSTANTS)

    @staticmethod
    def err_unknown_hostname(hostname):
        return f'unknown hostname', 404

    @staticmethod
    def get_machine_info_json(hostname):
        doc = db.get_machine_newest(hostname)
        if doc is None:
            return Views.err_unknown_hostname(hostname)
        doc['_id'] = str(doc['_id'])
        return jsonify(doc)

    @staticmethod
    def diff_machine_info(hostname):
        diff, states = db.get_machine_diff(hostname)

        if diff is None:
            # no diff to calculate
            return Views.get_machine_info_json(hostname)

        out = {
            'last_change': diff.copy(),
            'new_time': states[0]['timestamp'].strftime(current_config.TIME_FMT),
            'old_time': states[1]['timestamp'].strftime(current_config.TIME_FMT),
        }

        out['states'] = []
        for d in states:
            d['_id'] = str(d['_id'])
            out['states'].append(d)
        return jsonify(out)

    @staticmethod
    def healthcheck():
        return 'ok', 200

    @staticmethod
    def server_version():
        return version(), 200

    @staticmethod
    def update_machine():
        if not Views.auth_is_valid():
            return 'unauthorized', 401

        # read the msgpack body
        mt = request.mimetype
        if not mt.startswith('application/') and mt.endswith('msgpack'):
            return 'invalid mimetype', 400

        blob = request.data
        try:
            machine = msgpack_to_info(blob)
        except ValueError:
            return 'invalid msgpack body', 400
        # except ValidationError:
        #     return 'invalid body', 400
        
        # update
        resp = db.update_machine(machine)
        return resp, 200

app = make_app()
