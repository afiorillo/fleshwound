from datetime import datetime
from time import sleep

from requests import get, put, ConnectionError
from git import Repo

from fleshwound import version
from fleshwound.config import current_config, log
from fleshwound.schema import info_to_msgpack


def send_to_server():
    body = info_to_msgpack()
    url = f'{current_config.SERVER_URL}/api/update'
    headers = {
        'Authorization': f'Bearer {current_config.BEARER_KEY}',
        'Content-Type': 'application/x-msgpack'
    }
    try:
        return put(url, headers=headers, data=body)
    except ConnectionError:
        return None  # network issues, keep trying


def check_for_updates():
    try:
        url = f'{current_config.SERVER_URL}/api/server_version'
        log.info(f'checking for updates via `GET {url}`')
        resp = get(url)
        server_version = resp.text
        my_version = version()
        if server_version != my_version:
            log.info(f'client version {my_version} mismatches server {server_version}')
            r = Repo('.')
            if r.active_branch.name != 'master':
                log.warning(
                    'automatic updates are disabled for development branches')
                return
            log.debug('pulling this repository')
            # r.git.checkout(server_version)
            r.remotes.origin.pull()
            log.info(f'Updated repository to {my_version}. Restarting the service...')
            raise SystemExit(1)
        log.debug('no update needed')
    except ConnectionError:
        pass  # try again next time


def client_loop():
    log.info(f'version {version()}')
    log.info(f'posting to {current_config.SERVER_URL}')
    log.info(f'posting in {current_config.LOOP_INTERVAL} second intervals')
    if current_config.UPDATE_INTERVAL is not None:
        log.info(f'checking for updates every {current_config.UPDATE_INTERVAL} seconds')
        last_update_time = datetime.min

    try:
        while True:
            # check for an update first
            if (datetime.utcnow() - last_update_time).total_seconds() > current_config.UPDATE_INTERVAL:
                check_for_updates()
                last_update_time = datetime.utcnow()

            resp = send_to_server()
            if resp is None:
                log.error(f'Unable to contact server. Trying again...')
            else:
                log.info(
                    f'Response from Server: {resp.status_code} - {resp.text}')
            sleep(current_config.LOOP_INTERVAL)
    except KeyboardInterrupt:
        print('Caught CTRL+C, quitting...')
        return
    except SystemExit:
        log.info('Caught a SystemExit, quitting...')
        return
    except:
        log.exception('Error posting to server!')