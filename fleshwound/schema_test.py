
from marshmallow import pprint

from fleshwound.schema import collect_info

def test_collecting_info():
    print('\ntesting info collection...')
    pprint(collect_info(), indent=2)
