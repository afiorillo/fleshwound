from json import dumps

import fleshwound.internals as guts


def test_getters():
    metrics = dict(
        [(name, Klass) for name, Klass in guts.__dict__.items()
         if isinstance(Klass, type) and issubclass(Klass, guts._Metric)
         and Klass != guts._Metric and Klass.will_work()])

    print('\ntesting getters...')
    for name, Klass in metrics.items():
        print(f'{name}:  {Klass.will_work()}')
        print(f'{name}:  {dumps(Klass.get(), indent=2)}')
