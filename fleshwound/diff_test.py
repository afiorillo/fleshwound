from marshmallow import pprint

from fleshwound.diff import diff_dictionaries

def test_trivial_diff():
    from_dict = to_dict = {
        'foo': 'bar'
    }
    assert len(diff_dictionaries(from_dict, to_dict)) == 0

def test_simple_diff():
    from_dict = {
        'removed key': 'will delete this',
        'no change': 'no change',
        'modified key': 'will modify',
        'ignored key': 'this can change',
    }
    to_dict = {
        'no change': 'no change',
        'modified key': 'this was modified',
        'added key': 'this is a new key',
        'ignored key': 'it changed!',
    }

    diff = diff_dictionaries(from_dict, to_dict, ignore=('ignored key',))
    pprint(diff, indent=2)

    # make sure keys of changes actually were observed
    assert 'added' in diff
    assert 'added key' in diff['added']
    assert 'modified_from' in diff
    assert 'modified key' in diff['modified_from']
    assert 'modified_to' in diff
    assert 'modified key' in diff['modified_to']
    assert 'removed' in diff
    assert 'removed key' in diff['removed']
    assert 'ignored key' not in diff['modified_from']
    
    # the values should be as expected
    assert diff['modified_from']['modified key'] == 'will modify'
    assert diff['modified_to']['modified key'] == 'this was modified'

    # modified_from and modified_to should ALWAYS have the same keys/structure
    assert diff['modified_from'].keys() == diff['modified_to'].keys()

def test_deep_diff():
    from_dict = {
        'top': {
            'unchanged': 'constant',
            'will change': 'this will change',
            'lvl2': {
                'unchanged': 'constant',
                'will change': 'this changes!',
            }
        }
    }
    to_dict = {
        'top': {
            'unchanged': 'constant',
            'will change': 'this changed!',
            'lvl2': {
                'unchanged': 'constant',
                'will change': 'this changed!',
            }
        }
    }

    diff = diff_dictionaries(from_dict, to_dict)
    pprint(diff, indent=2)

    assert 'modified_from' in diff
    assert 'modified_to' in diff
    assert 'added' not in diff
    assert 'removed' not in diff
    assert 'top' in diff['modified_from'] and 'top' in diff['modified_to']
    assert 'unchanged' not in diff['modified_from']['top']
    assert 'will change' in diff['modified_from']['top']
    assert 'lvl2' in diff['modified_from']['top']
    assert 'unchanged' not in diff['modified_from']['top']['lvl2']
    assert 'will change' in diff['modified_from']['top']['lvl2']


if __name__ == '__main__':
    test_simple_diff()
    test_deep_diff()