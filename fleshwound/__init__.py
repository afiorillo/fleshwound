
from fleshwound.config import get_server_version

def version():
    return get_server_version()
        