import socket
import warnings

from marshmallow import Schema, fields, ValidationError, pprint
from marshmallow.warnings import ChangedInMarshmallow3Warning
import msgpack

import fleshwound.internals as guts

warnings.simplefilter('ignore', category=ChangedInMarshmallow3Warning)


class CPUInfo(Schema):
    arch = fields.String()
    model = fields.String()
    max_clock = fields.Float(allow_none=True)
    info = fields.List(fields.Dict(), allow_none=True)


class MemoryInfo(Schema):
    info = fields.List(fields.Dict(), allow_none=True)


class NetworkInfo(Schema):
    info = fields.List(fields.Dict(), allow_none=True)


class GPUInfo(Schema):
    cuda = fields.List(fields.Dict(), allow_none=True)
    info = fields.List(fields.Dict(), allow_none=True)


class MachineInfo(Schema):
    path = fields.List(fields.String())
    platform = fields.String()
    cpu_cores = fields.Int()
    total_memory = fields.Int()
    tdr_delay = fields.Float(allow_none=True)
    max_gpu_memory = fields.Int(allow_none=True)
    max_gpu_name = fields.String(allow_none=True)
    cpu = fields.Nested(CPUInfo)
    memory = fields.Nested(MemoryInfo)
    network = fields.Nested(NetworkInfo)
    gpu = fields.Nested(GPUInfo)


class SoftwareInfo(Schema):
    java_version = fields.String(allow_none=True)
    java_path = fields.String(allow_none=True)
    matlab91_root = fields.String(allow_none=True)
    matlab91_runtime = fields.String(allow_none=True)
    vcredist_2015_x64 = fields.String(allow_none=True)
    vcredist_2013_x64 = fields.String(allow_none=True)
    cuda_compiler_version = fields.String(allow_none=True)


class StatusPayload(Schema):
    # the minimal required to be valid
    hostname = fields.String(required=True, validate=guts.Hostname.is_valid)
    ip_address = fields.String(required=True, validate=guts.IPAddress.is_valid)
    # extra helpful information
    machine = fields.Nested(MachineInfo)
    software = fields.Nested(SoftwareInfo)


def collect_info(machine=True, software=True):
    info = {
        'hostname': guts.Hostname.get(),
        'ip_address': guts.IPAddress.get(),
    }

    if machine:
        info['machine'] = {
            'path': guts.EnvironPath.get(),
            'platform': guts.OperatingSystem.get(),
            'tdr_delay': guts.TDRDelay.get(),
            'cpu_cores': guts.CPUCores.get(),
            'total_memory': guts.TotalMemory.get(),
            'max_gpu_memory': guts.BestGPUMemory.get(),
            'max_gpu_name': guts.BestGPUName.get(),
            'cpu': {
                'arch': guts.CPUArch.get(),
                'model': guts.CPUModel.get(),
                'max_clock': guts.CPUClockSpeed.get(),
                'info': guts.CPUInfo.get(),
            },
            'memory': {
                'info': guts.MemoryInfo.get(),
            },
            'network': {
                'info': guts.NetworkInfo.get(),
            },
            'gpu': {
                'cuda': guts.GPUCudaInfo.get(),
                'info': guts.GPUSystemInfo.get(),
            }
        }

    if software:
        info['software'] = {
            'java_version': guts.JavaVersion.get(),
            'java_path': guts.JavaPath.get(),
            'matlab91_root': guts.Matlab91Root.get(),
            'matlab91_runtime': guts.Matlab91Runtime.get(),
            'vcredist_2015_x64': guts.VCRedist2015_x64.get(),
            'vcredist_2013_x64': guts.VCRedist2013_x64.get(),
            'cuda_compiler_version': guts.CudaCompilerVersion.get(),
            
        }

    data, errs = StatusPayload(strict=True).dump(info)
    if errs != {}:
        raise ValidationError(errs)
    return data


def info_to_msgpack(**kwargs):
    data = collect_info(**kwargs)
    return msgpack.packb(data, use_bin_type=True)

def msgpack_to_info(blob):
    unverified_data = msgpack.unpackb(blob, raw=False)
    data, errs = StatusPayload(strict=True).load(unverified_data)
    if len(errs) > 0:
        pprint(StatusPayload(strict=True).load(unverified_data))
        raise ValidationError(errs)
    return data

if __name__ == '__main__':
    collect_info()
