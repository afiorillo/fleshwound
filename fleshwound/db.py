from datetime import datetime

from marshmallow import ValidationError
from pymongo import MongoClient, DESCENDING

from fleshwound.diff import diff_dictionaries
from fleshwound.schema import StatusPayload
from fleshwound.config import current_config, log

DATABASE_NAME = 'fleshwound'
STATUSES_COLLECTION = 'machines'

_client = None

class DatabaseError(Exception): 
    pass


def get_client():
    global _client
    if _client is None:
        if current_config.MONGO_URL == '':
            raise DatabaseError('no mongo_host configured')
        _client = MongoClient(current_config.MONGO_URL)
    return _client


def get_database():
    client = get_client()
    return client.get_database(DATABASE_NAME)


def get_statuses_collection():
    db = get_database()
    return db.get_collection(STATUSES_COLLECTION)


def machine_list(projection=None):
    sort = [('_id', DESCENDING)]
    coll = get_statuses_collection()

    # get all machines ever pinged to the server
    machines = coll.distinct('hostname')
    # and find the most recent ping
    return [
        coll.find_one({'hostname': hostname}, projection=projection, sort=sort)
        for hostname in machines
    ]


def get_machine_newest(hostname, projection=None):
    coll = get_statuses_collection()
    return coll.find_one({'hostname': hostname},
                         projection=projection,
                         sort=[('_id', DESCENDING)])

DIFF_IGNORE_FIELDS = ('timestamp', '_id', 'wmic_cpu_info')

def get_machine_diff(hostname, projection=None):
    coll = get_statuses_collection()
    docs = list(
        coll.find({'hostname': hostname},
                  projection=projection,
                  sort=[('_id', DESCENDING)]))
    if len(docs) < 2:
        return None, None
    return diff_dictionaries(
        docs[1], docs[0], ignore=DIFF_IGNORE_FIELDS), docs


def update_machine(machine_data):
    machine_data, errs = StatusPayload(strict=True).load(machine_data)
    if errs != {}:
        raise ValidationError(errs)

    try:
        del machine_data['_id']
    except KeyError:
        pass  # just checking...
    machine_data['timestamp'] = datetime.utcnow()

    # only insert if it is different
    coll = get_statuses_collection()
    q = coll.find_one({'hostname': machine_data['hostname']}, sort=[('_id', DESCENDING)])

    if q is None or len(diff_dictionaries(q, machine_data, ignore=DIFF_IGNORE_FIELDS))>0:
        # it's different, insert a new record
        coll.insert_one(machine_data)
        return 'change detected'
    else:
        # it's not, replace the old one
        coll.replace_one({'_id': q['_id']}, machine_data)
        return 'accepted'
        