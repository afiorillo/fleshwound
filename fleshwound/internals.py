"""
Internals collects information about the machine
"""
from abc import ABC, abstractmethod
from distutils.version import LooseVersion  # noqa
import multiprocessing
import os
from pathlib import Path
import platform
import re
import socket
import string
import sys
import subprocess as proc
try:
    import winreg
except ImportError:
    pass  # we only need it on windows anyway

from marshmallow import fields

from fleshwound.config import log


class _Metric(ABC):
    @classmethod
    def get(cls):
        """ OS-independent (may raise NotImplementedError) getter """
        if not cls.will_work():
            return None
        elif sys.platform.startswith('linux'):
            return cls.get_linux()
        elif sys.platform.startswith('win32'):
            return cls.get_win()
        elif sys.platform.startswith('darwin'):
            return cls.get_osx()

    @staticmethod
    def get_win():
        raise NotImplementedError('not implemented on windows')

    @staticmethod
    def get_linux():
        raise NotImplementedError('not implemented on unix')

    @staticmethod
    def get_osx():
        raise NotImplementedError('not implemented on osx')

    @staticmethod
    def is_valid(value):
        """ Validating function for a value """
        return True

    @classmethod
    def will_work(cls):
        """ OS-independent (may raise NotImplementedError) checker if this metric works """
        if sys.platform.startswith('linux'):
            return cls.will_work_linux()
        elif sys.platform.startswith('win32'):
            return cls.will_work_win()
        elif sys.platform.startswith('darwin'):
            return cls.will_work_osx()

    @staticmethod
    def will_work_linux():
        return False

    @staticmethod
    def will_work_win():
        return False

    @staticmethod
    def will_work_osx():
        return False


class utils:
    @staticmethod
    def which(filename):
        """ Returns the full filepath of the given filename, if it's accessible on PATH. Else, returns None. """
        for dir in EnvironPath.get():
            fp = Path(dir).joinpath(filename)
            if fp.exists():
                return str(fp)

    @staticmethod
    def get_registry_value(rootKey, subKey, valueName):
        """ A shortcut for getting a registry value. """
        try:
            with winreg.OpenKey(rootKey, subKey) as key:
                valTup = winreg.QueryValueEx(key, valueName)
                return valTup[0]
        except OSError:
            pass  # the registry key isn't there
        except:
            log.debug(
                f'Unexpected error getting registry value ({rootKey},{subKey},{valueName})'
            )
            return None

    @staticmethod
    def parse_wmic(wmic_string):
        """ Parses a WMIC string (fixed width table) into a list of dictionaries. """
        if wmic_string is None: return None
        out = []
        try:
            lines = [
                l.strip() for l in wmic_string.split('\n') if l.strip() != ''
            ]
            header = lines[0]
            keys = header.split()
            keyPos = [header.index(k) for k in keys]
            for lIdx, line in enumerate(lines):
                if lIdx == 0: continue  # header
                row = {}
                for kIdx, key in enumerate(keys):
                    begPos = keyPos[kIdx]
                    try:
                        endPos = keyPos[kIdx + 1] - 1
                    except IndexError:
                        endPos = len(line)
                    val = line[begPos:endPos]
                    row[key] = val.strip()
                out.append(row)
            return out
        except:
            log.debug('Unable to parse WMIC result')
            return None

    @staticmethod
    def call_wmic(deviceName, keys):
        """ Calls WMIC and logs errors, returns unparsed output or None if failed. """
        try:
            out = proc.check_output(
                ['wmic', deviceName, 'get', ','.join(keys)],
                universal_newlines=True,
                text=True)
            return out
        except:
            log.debug('Unable to call WMIC process')


class Hostname(_Metric):
    """ Gets the hostname """

    @staticmethod
    def get():
        return platform.node()

    @staticmethod
    def will_work():
        return True


class IPAddress(_Metric):
    """ Gets the IP address (via `socket.gethostbyname`) """

    @staticmethod
    def get_win():
        try:
            info = NetworkInfo.get_win()
            return info[0]['ipAddress']
        except:
            return socket.gethostbyname(platform.node())

    @staticmethod
    def get_linux():
        return socket.gethostbyname(platform.node())

    @staticmethod
    def get_osx():
        return socket.gethostbyname(platform.node())

    @staticmethod
    def is_valid(value):
        try:
            socket.inet_aton(value)
            return True
        except socket.error:
            return False

    @staticmethod
    def will_work():
        return True


class EnvironPath(_Metric):
    @staticmethod
    def get():
        return sorted(list(set(os.environ.get('PATH', '').split(os.pathsep))))

    @staticmethod
    def will_work():
        return True


class OperatingSystem(_Metric):
    @staticmethod
    def get():
        return platform.platform()

    @staticmethod
    def will_work():
        return True


class JavaVersion(_Metric):
    """ Returns the version string for the installed version of Java, if available. """

    @staticmethod
    def get_win():
        try:
            o = proc.check_output(['java', '-version'],
                                  stderr=proc.STDOUT,
                                  universal_newlines=True,
                                  text=True)
            lines = [l.strip() for l in str(o).split('\n') if l.strip() != '']
            ver = lines[0].split(' version ')[-1]
            return ver.strip().strip('"').strip()
        except:
            log.debug('error checking for java')
            return None

    @staticmethod
    def will_work_win():
        return utils.which('java.exe') is not None


class JavaPath(_Metric):
    """ Returns the path to the installed version of Java, if available. """

    @staticmethod
    def get():
        for javaname in ['java.exe', 'java']:
            fp = utils.which(javaname)
            if fp is not None:
                return fp

    @staticmethod
    def will_work_win():
        return True


class Matlab91Root(_Metric):
    """ Returns the path to where MATLAB 9.1 (2016b) is installed, or None if it's not installed. """

    @staticmethod
    def get_win():
        return utils.get_registry_value(winreg.HKEY_LOCAL_MACHINE,
                                        r'SOFTWARE\Mathworks\MATLAB\9.1',
                                        'MATLABROOT')

    @staticmethod
    def will_work_win():
        return True


class Matlab91Runtime(_Metric):
    """ Returns the path to where MATLAB Compiler Runtime 9.1 is installed, or None if it's not installed. """

    @staticmethod
    def get_win():
        return utils.get_registry_value(
            winreg.HKEY_LOCAL_MACHINE,
            r'SOFTWARE\Mathworks\MATLAB Runtime\9.1', 'MATLABROOT')

    @staticmethod
    def will_work_win():
        return True


class VCRedist2015_x64(_Metric):
    """ Returns the version/build number for Visual Studio 2015 x64 Redistributables. """

    @staticmethod
    def get_win():
        val = None
        for tup in [
            (winreg.HKEY_LOCAL_MACHINE,
             r'SOFTWARE\Classes\Installer\Dependencies\{d992c12e-cab2-426f-bde3-fb8c53950b0d}',
             'Version'),
            (winreg.HKEY_LOCAL_MACHINE,
             r'SOFTWARE\Classes\Installer\Dependencies\{e46eca4f-393b-40df-9f49-076faf788d83}',
             'Version'),
        ]:
            val = utils.get_registry_value(tup[0], tup[1], tup[2])
            if val is not None:
                return val

    @staticmethod
    def will_work_win():
        return True


class VCRedist2013_x64(_Metric):
    """ Returns the version/build number for Visual Studio 2013 x64 Redistributables. """

    @staticmethod
    def get_win():
        val = None
        for tup in [
            (winreg.HKEY_LOCAL_MACHINE,
             r'SOFTWARE\Classes\Installer\Dependencies\{050d4fc8-5d48-4b8f-8972-47c82c46020f}',
             'Version'),
            (winreg.HKEY_LOCAL_MACHINE,
             r'SOFTWARE\Classes\Installer\Dependencies\{7f51bdb9-ee21-49ee-94d6-90afc321780e}',
             'Version'),
        ]:
            val = utils.get_registry_value(tup[0], tup[1], tup[2])
            if val is not None:
                return val

    @staticmethod
    def will_work_win():
        return True


class CudaCompilerVersion(_Metric):
    """ Returns the version/build of the CUDA Compiler Driver, if available. """

    @staticmethod
    def get_win():
        try:
            out = proc.check_output(['nvcc', '--version'], universal_newlines=True, text=True)
            ver = LooseVersion(out.split('V')[-1].strip())
            return str(ver)
        except:
            log.debug('unable to get CUDA Compiler version')

    @staticmethod
    def will_work_win():
        return utils.which('nvcc.exe') is not None


class TDRDelay(_Metric):
    """ Returns the current setting for TDR delay. """

    @staticmethod
    def get_win():
        try:
            val = utils.get_registry_value(
                winreg.HKEY_LOCAL_MACHINE,
                r'System\CurrentControlSet\Control\GraphicsDrivers',
                'TdrDelay')
            if val is not None:
                return int(val)
        except:
            log.debug('unable to get TdrDelay')

    @staticmethod
    def will_work_win():
        return True


class CPUArch(_Metric):
    """ Returns the CPU architecture (e.g. i386, AMD64). """

    @staticmethod
    def get():
        return platform.machine()

    @staticmethod
    def will_work():
        return True


class CPUModel(_Metric):
    """ Returns the CPU information. """

    @staticmethod
    def get():
        return platform.processor()

    @staticmethod
    def will_work():
        return True


class CPUCores(_Metric):
    """ Return the number of cores in the CPU. """

    @staticmethod
    def get():
        return multiprocessing.cpu_count()

    @staticmethod
    def will_work():
        return True


class CPUInfo(_Metric):
    """ Returns any available information about the installed CPU(s) """

    @staticmethod
    def get_win():
        WMIC_KEYS = [
            'Description', 'CurrentClockSpeed', 'L2CacheSize', 'L3CacheSize',
            'Family', 'MaxClockSpeed', 'Name', 'NumberOfCores',
            'NumberOfLogicalProcessors', 'SocketDesignation'
        ]
        deviceName = 'cpu'
        out = utils.call_wmic(deviceName, WMIC_KEYS)
        cpuInfo = utils.parse_wmic(out)
        for cpu in cpuInfo:
            for intfield in [
                    'CurrentClockSpeed',
                    'MaxClockSpeed',
                    'L2CacheSize',
                    'L3CacheSize',
                    'NumberOfCores',
                    'NumberOfLogicalProcessors',
            ]:
                cpu[intfield] = int(cpu.get(intfield, -1))
        return cpuInfo

    @staticmethod
    def will_work_win():
        return True


class CPUClockSpeed(_Metric):
    """ Return the max clock speed (in MHz) of the installed CPU. """

    @staticmethod
    def get_win():
        cpuInfo = CPUInfo.get_win()
        if cpuInfo is None: return

        maxClock = 0
        for cpu in cpuInfo:
            cpuMax = cpu.get('MaxClockSpeed')
            if cpuMax > maxClock: maxClock = cpuMax
        if maxClock > 0:
            return maxClock

    @staticmethod
    def will_work_win():
        return True


class NetworkInfo(_Metric):
    """ Returns any available information about the installed network adapters """

    @staticmethod
    def get_win():
        WMIC_KEYS = [
            'Description', 'IPAddress', 'MACAddress'
        ]
        PRIORITY_IP_PREFIXES = ('192.168.0', '192.168.4')
        deviceName = 'nicconfig'
        out = utils.call_wmic(deviceName, WMIC_KEYS)
        netAdapters = utils.parse_wmic(out)
        netInfo = []
        for adapter in netAdapters:
            ipaddress = adapter.get('IPAddress', '').strip('{}"')
            obj = {
                'name': adapter.get('Description'),
                'ipAddress': ipaddress,
                'macAddress': adapter.get('MACAddress'),
            }
            if ipaddress == "":
                continue
            elif any(ipaddress.startswith(prefix) for prefix in PRIORITY_IP_PREFIXES):
                netInfo.insert(0, obj)
            else:
                netInfo.append(obj)
        return netInfo

    @staticmethod
    def will_work_win():
        return True


class MemoryInfo(_Metric):
    """ Returns all available information about installed memory. """

    @staticmethod
    def get_win():
        WMIC_KEYS = [
            'Capacity', 'DeviceLocator', 'Manufacturer', 'PartNumber', 'Speed'
        ]
        deviceName = 'memorychip'
        out = utils.call_wmic(deviceName, WMIC_KEYS)
        memInfo = utils.parse_wmic(out)
        for dimm in memInfo:
            for intfield in [
                    'Capacity',
                    'Speed',
            ]:
                dimm[intfield] = int(dimm.get(intfield, -1))
        return memInfo

    @staticmethod
    def will_work_win():
        return True


class TotalMemory(_Metric):
    """ Return the total physical memory installed (in MB). """

    @staticmethod
    def get_win():
        memInfo = MemoryInfo.get()
        if memInfo is None: return

        totalMem = 0
        for mem in memInfo:
            try:
                totalMem += int(mem.get('Capacity'))
            except ValueError:
                pass
        if totalMem > 0:
            return int(totalMem / (10**6))

    @staticmethod
    def will_work_win():
        return True


class GPUCudaInfo(_Metric):  # TODO broken
    """ Returns all available information about the installed GPU(s) """

    @staticmethod
    def get_win():
        info = []
        nvcc_exc = utils.which('nvcc.exe')
        if nvcc_exc is None:
            return None
        query_exc = Path(nvcc_exc).joinpath('../../extras/demo_suite/deviceQuery.exe')
        try:
            query_exc = query_exc.resolve()
        except OSError:
            print(f'unable to query gpu: "{query_exc} does not exist')
            return None
        query_dir = query_exc.parent
        
        try:
            cmd = ['cmd', '/C', 'deviceQuery.exe']
            out = proc.check_output(cmd, cwd=query_dir, universal_newlines=True, text=True)
            lines = [l.strip() for l in out.split('\n') if l.strip() != '']
            device_exp = re.compile('Device [0-9]')
            device_idx = [idx for idx, txt in enumerate(lines) if device_exp.match(txt)]
            for indx in device_idx:
                device = {
                    'deviceName': lines[indx].split(':')[-1].strip().strip('"')
                }
                line = indx + 1
                while line not in device_idx and line < len(lines):
                    # not to the next device yet
                    parts = lines[line].split('  ')
                    if len(parts) >= 2:
                        name = parts[0].strip()
                        value = parts[-1].strip()
                        device[name] = value
                    line += 1
                info.append(device)
            return info
        except proc.CalledProcessError:
            log.exception('error getting gpu info')
        except:
            log.exception('unable to get gpu info')

    @staticmethod
    def will_work_win():
        return utils.which('nvcc.exe') is not None


class GPUSystemInfo(_Metric):
    """ Returns the list of model names for each graphics card installed in the machine. """

    @staticmethod
    def get_win():
        WMIC_KEYS = ['name', 'DriverVersion']
        try:
            out = proc.check_output([
                'wmic', 'path', 'win32_VideoController', 'get',
                ','.join(WMIC_KEYS),
            ], universal_newlines=True, text=True)
            return utils.parse_wmic(out)
        except:
            log.debug('unable to get gpu system info')

    @staticmethod
    def will_work_win():
        return True


class BestGPUMemory(_Metric):
    """ Returns the total physical memory (in MB) of the best GPU installed. """

    @staticmethod
    def get_win():
        gpuInfo = GPUCudaInfo.get()
        if gpuInfo is None: return

        maxMem = 0
        for gpu in gpuInfo:
            try:
                gpu_keys = gpu.keys()
                memkey = [k for k in gpu_keys if 'amount of global memory' in k][0]
                memstr = gpu[memkey]
                gpuMem = int(memstr.split('MBytes')[0].strip())
            except (ValueError, IndexError):
                pass
            else:
                if gpuMem > maxMem: maxMem = gpuMem
        if maxMem > 0:
            return maxMem

    @staticmethod
    def will_work_win():
        return GPUCudaInfo.will_work_win()


class BestGPUName(_Metric):
    """ Returns the name of the best GPU installed. """

    @staticmethod
    def get_win():
        gpuInfo = GPUCudaInfo.get()
        if gpuInfo is None: return

        maxMem = 0
        maxName = None
        for gpu in gpuInfo:
            try:
                gpu_keys = gpu.keys()
                memkey = [k for k in gpu_keys if 'amount of global memory' in k][0]
                memstr = gpu[memkey]
                gpuMem = int(memstr.split('MBytes')[0].strip())
            except (ValueError, IndexError):
                pass
            else:
                if gpuMem > maxMem:
                    maxMem = gpuMem
                    maxName = gpu.get('deviceName')
        return maxName

    @staticmethod
    def will_work_win():
        return GPUCudaInfo.will_work_win()
