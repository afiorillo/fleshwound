import logging
import os

from dateutil import tz
from git import Repo


class BaseConfig(object):
    _NAME = None
    LOG_LEVEL = int(os.environ.get('FLESHWOUND_LOG_LEVEL', logging.INFO))
    # a secret key clients must present to the server to update
    BEARER_KEY = os.environ.get('FLESHWOUND_BEARER_KEY', '')

class ServerConfig(BaseConfig):
    _NAME = 'server'

     # mongo configuration
    MONGO_URL = os.environ.get('FLESHWOUND_MONGO_URL', 'mongodb://127.0.0.1:27017/')
    # the format to print
    TIME_FMT = '%Y-%m-%d %H:%M:%S EST'
    # the time zone to localize timestamps
    TIME_ZONE = tz.gettz('Eastern Standard Time')
    # these are passed to all jinja templates at render as "constants"
    JINJA_CONSTANTS = {
        'ERROR_POLL_TIMEOUT': 600,
    }


class ClientConfig(BaseConfig):
    _NAME = 'client'

    # client needs to find the server
    SERVER_URL = os.environ.get('FLESHWOUND_SERVER_URL', 'http://127.0.0.1:8000')
    # the number of seconds between posts
    LOOP_INTERVAL = int(os.environ.get('FLESHWOUND_LOOP_INTERVAL', 30))
    # the number of seconds between checking for updates
    UPDATE_INTERVAL = int(os.environ.get('FLESHWOUND_UPDATE_INTERVAL', 60*60*6))  # every 6 hours
    # git repository (for automatic updates)
    GIT_REPOSITORY = 'git@gitlab.com:afiorillo/fleshwound.git'


def get_config(name=None):
    if name is None:
        name = os.environ.get('FLESHWOUND_ENV_NAME', 'client')

    def _validate(Cfg):
        pass

    for Cfg in [ServerConfig, ClientConfig]:
        if Cfg._NAME == name:
            _validate(Cfg)
            return Cfg

    
    raise ValueError(f'invalid config name {name}')


__server_version = None
def get_server_version():
    """ return the current commit's SHA-1 """
    global __server_version
    if not __server_version:
        __server_version = Repo('.').commit().hexsha
    return __server_version


current_config = get_config()
log = logging.getLogger('fleshwound')
logging.basicConfig(format='%(asctime)s %(name)s %(message)s')
log.setLevel(current_config.LOG_LEVEL)
# TODO: log writing