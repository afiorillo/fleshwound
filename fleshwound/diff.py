from collections import defaultdict

def _diff_dicts(from_dict: dict, to_dict: dict, ignore=()):
    """ recursively differences dictionaries """
    added = {}
    removed = {}
    modified_from = {}
    modified_to = {}

    if not (isinstance(from_dict, dict) and isinstance(to_dict, dict)):
        modified_from = from_dict
        modified_to = to_dict
        return added, removed, modified_from, modified_to

    from_keys = set(from_dict.keys()) - set(ignore)
    to_keys = set(to_dict.keys()) - set(ignore)
    added_keys = to_keys - from_keys
    removed_keys = from_keys - to_keys
    shared_keys = (to_keys & from_keys)

    for key in shared_keys:
        from_val = from_dict[key]
        to_val = to_dict[key]
        if from_val == to_val:
            continue
            
        key_added, key_removed, key_modified_from, key_modified_to = _diff_dicts(from_val, to_val)
        if len(key_added) > 0:
            added[key] = key_added
        if len(key_removed) > 0:
            removed[key] = key_removed
        if len(key_modified_from) > 0:
            modified_from[key] = key_modified_from
        if len(key_modified_to) > 0:
            modified_to[key] = key_modified_to

    for key in added_keys:
        added[key] = to_dict[key]
    
    for key in removed_keys:
        removed[key] = from_dict[key]

    return added, removed, modified_from, modified_to

def diff_dictionaries(from_dict: dict, to_dict: dict, ignore=()):
    """ Determines the difference between two dictionaries """
    diff = {}
    added, removed, modified_from, modified_to = _diff_dicts(from_dict, to_dict, ignore)
    if len(added) > 0:
        diff['added'] = added
    if len(removed) > 0:
        diff['removed'] = removed
    if len(modified_from) > 0:
        diff['modified_from'] = modified_from
    if len(modified_to) > 0:
        diff['modified_to'] = modified_to

    return diff