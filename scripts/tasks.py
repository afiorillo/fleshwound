#!/usr/bin/python

from invoke import task

@task
def devserver(c):
    """starts an ephermeral development server"""

    # start mongo
    result = c.run('docker run -d --name "invoke-dev-server" --rm mongo:3.4.1')
    container_id = result.stdout

    # start flask with restart
    with c.cd('..'):
        c.run('pipenv run server')

    # kill mongo
    c.run(f'docker stop {container_id}')
